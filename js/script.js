'use strict'
let userNum = null;
userNum = Number(prompt("Please enter your number"));
while (!userNum || isNaN(userNum)) {
  alert('Invalid. Please enter number!');
  userNum = prompt("Please enter your number");
}

if (userNum <= 4) {
  console.log('Sorry, no numbers');
}

for (let i = 0; i <= userNum; i++) {

  if (i % 5 === 0 && i !== 0) {
    console.log(i);
  }
}


// Питання:
// 1)Цикли використовуються у випадку коли необхідно використати дію необхідну кількість разів.
// Цикли часто використовуються для роботи з масивами, коли необхідно звернутися до кожного чи декількох елеметів масиву.
// 2)Використав while у другому ДЗ
// 3)Перетворення типу схоже на приведення типу, тому що вони обидва перетворюють значення з одного типу даних в інший з однією ключовою відмінністю приведення типу є неявним, тоді як перетворення типу може бути неявним або явним.